# kotlin-test-dsl

A DSL for testing with Kotlin using, short, concise and descriptive tests. Extends the Behavior Spec in the Kotest test framework.

## License

This project is licensed under the MIT License, you can read the full text [here](LICENSE)
