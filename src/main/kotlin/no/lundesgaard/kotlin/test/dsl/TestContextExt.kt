package no.lundesgaard.kotlin.test.dsl

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.core.test.TestContext

private val scopeVariablesMap = mutableMapOf<TestContext, MutableMap<String, Any?>>()

var TestContext.scopeVariables: MutableMap<String, Any?>
    get() = scopeVariablesMap.getOrPut(this) { mutableMapOf() }
    set(value) { scopeVariablesMap[this] = value }

fun TestContext.save(context: BehaviorSpec) {
    scopeVariables = context.scopeVariables.toMutableMap()
}

fun TestContext.save(context: TestContext) {
    scopeVariables = context.scopeVariables.toMutableMap()
}
