package no.lundesgaard.kotlin.test.dsl

import io.kotest.core.test.TestContext

val describedTests = mutableMapOf<String, DescribedTest>()

data class DescribedTest(
    val description: String,
    val test: TestContext.() -> Unit,
)

fun String.describes(execute: TestContext.() -> Unit): DescribedTest {
    val describedTest = DescribedTest(this, execute)
    describedTests[this] = describedTest
    return describedTest
}

fun String.toTest(): DescribedTest = describedTests[this] ?: error("unknown test: $this")

operator fun String.invoke() = toTest()