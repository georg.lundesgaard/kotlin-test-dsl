package no.lundesgaard.kotlin.test.dsl

import io.kotest.core.spec.style.scopes.BehaviorSpecGivenContainerContext
import io.kotest.core.spec.style.scopes.BehaviorSpecWhenContainerContext

suspend fun BehaviorSpecGivenContainerContext.And(
    describedTest: DescribedTest,
    additionalTest: suspend BehaviorSpecGivenContainerContext.() -> Unit,
) = let { context ->
    describedTest.apply {
        And(description) {
            save(context)
            test()
            additionalTest()
        }
    }
}

suspend fun BehaviorSpecGivenContainerContext.When(
    describedTest: DescribedTest,
    additionalTest: suspend BehaviorSpecWhenContainerContext.() -> Unit,
) = let { context ->
    describedTest.apply {
        When(description) {
            save(context)
            test()
            additionalTest()
        }
    }
}