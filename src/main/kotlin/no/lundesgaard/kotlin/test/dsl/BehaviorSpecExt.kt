package no.lundesgaard.kotlin.test.dsl

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.core.spec.style.scopes.BehaviorSpecGivenContainerContext

private val scopeVariablesMap = mutableMapOf<BehaviorSpec, MutableMap<String, Any?>>()

val BehaviorSpec.scopeVariables: MutableMap<String, Any?>
    get() = scopeVariablesMap.getOrPut(this) { mutableMapOf() }

fun BehaviorSpec.GIVEN(
    testName: String,
    additionalTest: suspend BehaviorSpecGivenContainerContext.() -> Unit,
) = Given(testName.toTest(), additionalTest)

fun BehaviorSpec.Given(
    describedTest: DescribedTest,
    additionalTest: suspend BehaviorSpecGivenContainerContext.() -> Unit,
) = let { context ->
    describedTest.apply {
        Given(description) {
            save(context)
            test()
            additionalTest()
        }
    }
}