package no.lundesgaard.kotlin.test.dsl

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.core.test.TestContext
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class ScopeVariables<T, V: Any?> : ReadWriteProperty<T, V> {
    override fun getValue(thisRef: T, property: KProperty<*>): V {
        val scopeVariables = when (thisRef) {
            is BehaviorSpec -> thisRef.scopeVariables
            is TestContext -> thisRef.scopeVariables
            else -> error("unknown context: $thisRef")
        }
        if (scopeVariables.containsKey(property.name)) {
            @Suppress("UNCHECKED_CAST")
            return scopeVariables[property.name] as V
        }
        error("Scope variable with name ${property.name} has not been initialized before get.")
    }

    override fun setValue(thisRef: T, property: KProperty<*>, value: V) {
        val scopeVariables = when (thisRef) {
            is BehaviorSpec -> thisRef.scopeVariables
            is TestContext -> thisRef.scopeVariables
            else -> error("unknown context: $thisRef")
        }
        scopeVariables[property.name] = value
    }
}