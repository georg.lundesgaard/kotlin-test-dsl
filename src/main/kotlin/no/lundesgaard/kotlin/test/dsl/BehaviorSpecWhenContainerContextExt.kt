package no.lundesgaard.kotlin.test.dsl

import io.kotest.core.spec.style.scopes.BehaviorSpecWhenContainerContext
import io.kotest.core.test.TestContext

suspend fun BehaviorSpecWhenContainerContext.And(
    describedTest: DescribedTest,
    additionTest: suspend BehaviorSpecWhenContainerContext.() -> Unit,
) = let { context ->
    describedTest.apply {
        And(description) {
            save(context)
            test()
            additionTest()
        }
    }
}

suspend fun BehaviorSpecWhenContainerContext.Then(
    describedTest: DescribedTest,
    additionalTest: suspend TestContext.() -> Unit = { },
) = let { context ->
    describedTest.apply {
        Then(description) {
            save(context)
            test()
            additionalTest()
        }
    }
}