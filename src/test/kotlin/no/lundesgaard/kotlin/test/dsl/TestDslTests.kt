package no.lundesgaard.kotlin.test.dsl

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.core.test.TestContext
import io.kotest.matchers.shouldBe

class TestDslTests : BehaviorSpec({
    z = 11

    GIVEN("x with value 1") {
        And(`y with value 2`) {
            When(`adding x and y`) {
                Then(`the result is 3`)

                And(`multiplying by z`) {
                    Then(`the result is 33`)
                }
            }
        }

        And(`y with value 3`) {
            When(`adding x and y`) {
                And(`multiplying by z`) {
                    Then(`the result is 44`)
                }
            }
        }

        And(`y is null`) {
            When(`adding x and y`) {
                Then(`the result is 1`)
            }
        }
    }
})

val `x with value 1` = "x with value 1".describes { x = 1 }
val `y with value 2` = "y with value 2".describes { y = 2 }
val `y with value 3` = "y with value 3".describes { y = 3 }
val `y is null` = "y is null".describes { y = null }
val `adding x and y` = "adding x and y".describes { result = x + (y ?: 0) }
val `multiplying by z` = "multiplying by z".describes { result *= z }
val `the result is 1` = "the result is 1".describes { result shouldBe 1 }
val `the result is 3` = "the result is 3".describes { result shouldBe 3 }
val `the result is 33` = "the result is 33".describes { result shouldBe 33 }
val `the result is 44` = "the result is 44".describes { result shouldBe 44 }

private var BehaviorSpec.z: Int by ScopeVariables()
private var TestContext.x: Int by ScopeVariables()
private var TestContext.y: Int? by ScopeVariables()
private var TestContext.z: Int by ScopeVariables()
private var TestContext.result: Int by ScopeVariables()
